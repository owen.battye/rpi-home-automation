#!/usr/bin/env python3

import socket
import os
import time

print("""Start demo client - send temperature to server

Press Ctrl+C to exit!

""")

def findServer(serverName, serverPort):
    print ("Searching for " + serverName)
    serverConnection = socket.socket()
    print("on port ", serverPort)
    
    serverConnection.connect((serverName, serverPort))
    received = serverConnection.recv(8)
    if received == b"12345":
        return serverConnection
            
    print ("Did not recognise server response")
    serverConnection.close()
        
if __name__ == "__main__":
    #displayTemperature()
    serverConnection = findServer(socket.gethostname(), 2002)
    clientConnected = True

    while clientConnected:
        try:
            temp = '{:s},{:s}'.format("demoClient1", "22.22")
            print ("Updated:", temp)
            serverConnection.send(bytes(temp, 'utf-8'))
            time.sleep(5)
        except KeyboardInterrupt:
            clientConnected = False
            pass
        except socket.error:
            break
        
    serverConnection.close()
    if (clientConnected):
        print ("Server stopped")
    else:
        print ("Demo client completed")
    