#!/usr/bin/env python3

import socket
import os
import time
#import struct
from bmp280 import BMP280

print("""Start client - send temperature to server

Press Ctrl+C to exit!

""")

try:
    from smbus2 import SMBus
except ImportError:
    from smbus import SMBus

def getTemperature(bmp280):
    temperature = bmp280.get_temperature()
    pressure = bmp280.get_pressure()
    #print('{:05.2f}*C {:05.2f}hPa'.format(temperature, pressure))
    return temperature
    
def findServer(serverName, serverPort):
    print ("Searching for " + serverName)
    serverConnection = socket.socket()
    print("Trying port ", serverPort)
    serverConnection.connect((serverName, serverPort))
    received = serverConnection.recv(8)
    print (received)
    if received == b"12345":
        return serverConnection
            
    print ("Could not connect")
    serverConnection.close()
        
if __name__ == "__main__":
    #displayTemperature()
    serverConnection = findServer(socket.gethostname(), 2002)
    bus = SMBus(1)
    bmp280 = BMP280(i2c_dev=bus)
    
    clientConnected = True

    while clientConnected:
        try:
            temp = '{:s},{:05.2f}'.format(socket.gethostname(), getTemperature(bmp280))
            print ("Temperature", temp)
            serverConnection.send(bytes(temp, 'utf-8'))
            time.sleep (5)
        except KeyboardInterrupt:
            clientConnected = False
            pass
        except socket.error:
            break
        
    serverConnection.close()
    if (clientConnected):
        print ("Server stopped")
    else:
        print ("Demo client completed")
    