#!/usr/bin/env python3

import socket
import os
import time
import threading
from demo_opts import get_device
from luma.core.virtual import terminal
from PIL import ImageFont

print("""Displays the temperature and for any number of clients

Press Ctrl+C to exit!

""")

def makeFont(name, size):
    font_path = os.path.abspath(os.path.join(
        os.path.dirname(__file__), 'fonts', name))
    return ImageFont.truetype(font_path, size)

def displayClientData(temperatureAndClient):
    print (temperatureAndClient)
    tempAndClient = temperatureAndClient.split(',', 2)
    
    print('\r{} {}*C'.format(tempAndClient[0], tempAndClient[1]))
    term.println('{client}: {temp}*C'.format(client = tempAndClient[0], temp = tempAndClient[1]))
    
def findClient():
    while True:
        try:
            s.listen(5)                 # Now wait for client connection.
            c, addr = s.accept()        # Establish connection with client.
            print ('Got connection from', addr)
            c.send(b'12345')
            clients.append("Unknown,0")
            threading.Thread(target=readClient, args=(c,len(clients)-1)).start()
        except socket.timeout as e:
            pass
        except socket.error:
            break

    s.close()
    print("Stopped looking for new clients")
    
def readClient(client, index):
    clientSockets.append(client)
    
    while True:
        print ("Updating client", index)
        try:
            receivedData = client.recv(20).decode("utf-8")
            if not receivedData: break
            clients[index] = receivedData
        except socket.error:
            break
        
    clients[index] = "Closed,0"
    client.close()
    print("Client stopped", index)
   
if __name__ == "__main__":
    port = 2002
    print ("Opening port ", port)
    s = socket.socket()
    s.settimeout(2.0)
    s.bind((socket.gethostname(), port))
    clients = []
    clientSockets = []

    continueProcess = True
    clientListenThread = threading.Thread(target=findClient)
    clientListenThread.start()
    font = makeFont("tiny.ttf", 12)
    device = get_device()
    term = terminal(device, font)
    term.animate = False

    while continueProcess:
        try:
            term.clear()
            term.println("Temperatures")
            for client in clients:
                displayClientData (client)
            time.sleep(5)
        except KeyboardInterrupt:
            continueProcess = False
            pass
    
    continueProcess = False
    s.close()
    print ("Socket closed")
    for client in clientSockets:
        print ("Stopping a client")
        client.close()

    print ("Server complete")
    